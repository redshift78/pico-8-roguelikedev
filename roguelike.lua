function _init()
  xpos = 8
  ypos = 8
end

function _update()
  -- left
  if btnp(0) and xpos > 0 then
    xpos = xpos - 1
  end

  -- right
  if btnp(1) and xpos < 15 then
    xpos = xpos + 1
  end

  -- up
  if btnp(2) and ypos > 0 then
    ypos = ypos - 1
  end

  -- down
  if btnp(3) and ypos < 15 then
    ypos = ypos + 1
  end
end

function _draw()
  cls()
  line(0,0,127,0,5)
  line(0,127,127,127,5)
  line(0,0,0,127,5)
  line(127,0,127,127,5)

  map(0,0,0,0,4,4)

  spr(0,xpos*8,ypos*8)
end
